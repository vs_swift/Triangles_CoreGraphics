//
//  ViewController.swift
//  triangles
//
//  Created by Private on 2/25/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var bezView: BezView!
    
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var heightCOnstraint: NSLayoutConstraint!
    
    @IBAction func sliderW(_ sender: UISlider) {
        widthConstraint.constant = CGFloat(sender.value)
        self.view.layoutIfNeeded()
        bezView.setNeedsDisplay()
    }
    
    @IBAction func sliderH(_ sender: UISlider) {
        heightCOnstraint.constant = CGFloat(sender.value)
        self.view.layoutIfNeeded()
        bezView.setNeedsDisplay()
    }
}

